FROM ruby:2.1

RUN mkdir -p /var/opt/crm /home/crm

RUN apt-get update && \
    apt-get -y install libmysqlclient-dev zip

RUN groupadd crm && \
    useradd crm -g crm -s /bin/bash

RUN chown -R crm: /var/opt /home/crm

RUN gem install bundler --no-ri --no-rdoc -v '~>1'

ADD Gemfile var/opt/crm/
ADD Gemfile.lock var/opt/crm/

RUN cd /var/opt/crm && bundle install

ADD bin var/opt/crm/bin
ADD config var/opt/crm/config
ADD lib var/opt/crm/lib
ADD public var/opt/crm/public
ADD views var/opt/crm/views
ADD config.ru var/opt/crm/

RUN chown -R crm: /var/opt /home/crm

USER crm

ENTRYPOINT ["/var/opt/crm/bin/start_crm"]
