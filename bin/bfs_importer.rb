#!/usr/bin/env ruby
require 'active_record'
require 'pp'
require_relative '../lib/nila_crm/db'
require 'csv'

include NilaCrm::Db

# concat subject columns and delete unused info
def preProcess(row)
  row2process = row

  # Concatenate multipe columns to one
  subject = row2process['Verwendungszweck'].to_s

  # Ensure at least one whitespace before keywords
  subject = subject.gsub('CRED+', ' CRED+')
  subject = subject.gsub('KREF+', ' KREF+')
  subject = subject.gsub('MREF+', ' MREF+')
  subject = subject.gsub('EREF+', ' EREF+')
  subject = subject.gsub('SVWZ+', ' SVWZ+')

  # Remove those keywords and the characters til the next whitespace
  subject = subject.gsub(/EREF:\s+[^\s]*/, '')
  subject = subject.gsub(/CRED:\s+[^\s]*/, '')
  subject = subject.gsub(/IBAN:\s+[^\s]*/, '')
  subject = subject.gsub(/BIC:\s+[^\s]*/, '')
  subject = subject.gsub(/MREF:\s+[^\s]*/, '')
  subject = subject.gsub(/KREF:\s+[^\s]*/, '')
  subject = subject.gsub(/\/\*DA-\d+\*/, '')
  subject = subject.gsub(/SVWZ\+/, '')
  subject = subject.gsub(/SignDate[\d-]*/, '')

  subject = subject.squeeze(' ')

  row2process['xxxSubject'] = truncate subject


  row2process['xxxName'] = row['Name Zahlungsbeteiligter'].to_s

  row2process['xxxBuchungsdatum'] = Date.strptime(row2process['Buchungstag'].to_str, '%d.%m.%Y')

  # Change decimal separator
  row2process['xxxBetrag'] = row['Betrag'].to_s.gsub(',', '.').to_f

  row2process['xxxBuchungstext'] = row['Buchungstext'].to_s.gsub('ä','ae').gsub('ö','oe').gsub('ü','ue').gsub('Ä','Ae').gsub('Ö','Oe').gsub('Ü','Ue').gsub('ß','ss')

  return row2process
end

def truncate s, length = 250, ellipsis = '...'
  if s.length > length
    s.to_s[0..length].gsub(/[^\w]\w+\s*$/, ellipsis)
  else
    s
  end
end


def cachedContacts()
  sql = "select key1, value1 from accountingimportsassignments where type = 'contact'"
  keyvalue = ActiveRecord::Base.connection.execute(sql)
  staticAssignments = Hash.new(0)

  keyvalue.each(:as => :hash) do |val|
    staticAssignments[val['key1']] = val['value1']
  end

  return staticAssignments
end

def determineKontakt(row)
  privateKontakte = determinePrivatperson(row)
  
	if privateKontakte && privateKontakte.size > 0
		return privateKontakte
	end
	return determineJuristischePerson(row)
end 

def determinePrivatperson(row)
  name = row['xxxName']
  return nil if name.blank?

  # Don't assign contacts automatically when amount lower 0
  betrag = row['xxxBetrag'].to_f
  return nil if betrag < 0

  kontaktNr = $staticContacts[name]

  if kontaktNr == 0
    lowerName = name.to_s.delete(' ').downcase
    whereClause = "instr('"+ lowerName +"', replace(lower(vorname), ' ', '')) > 0 AND "
    whereClause += "instr('"+ lowerName +"', replace(lower(name), ' ', '')) > 0 AND "
    whereClause += "typus = 'Privatperson' AND "
    whereClause += "nilastate_id IN (2, 3, 4, 5)"
    #whereClause += " OR "
    #whereClause += "instr('"+ lowerName +"', replace(lower(se_name), ' ', '')) > 0 AND "
    #whereClause += "nilastate_id IN (7, 8, 10)"
  else
    whereClause = "ID = #{kontaktNr}"
  end

  return Contact.where(whereClause)
end

def determineJuristischePerson(row)
  name = row['xxxName']
  return nil if name.blank?

  # Don't assign contacts automatically when amount lower 0
  betrag = row['xxxBetrag'].to_f
  return nil if betrag < 0

  kontaktNr = $staticContacts[name]

  if kontaktNr == 0
    lowerName = name.to_s.delete(' ').downcase
    whereClause = "trim(coalesce(se_name, '')) <> '' AND "
    whereClause += "instr('"+ lowerName +"', replace(lower(se_name), ' ', '')) > 0 AND "
    whereClause += "typus = 'Juristische Person' AND "
    whereClause += "nilastate_id IN (7, 8, 10)"
  else
    whereClause = "ID = #{kontaktNr}"
  end
   
  return Contact.where(whereClause)
end

def subjectMatch(subject, matches)

  lowercaseSubject = subject.to_s.downcase

  matches.each { |pattern|
    return true if lowercaseSubject.match(/#{Regexp.escape pattern}/)
  }
  return false
end

def classifyVorgang(row, contact)

  betrag = row['xxxBetrag'].to_f
  subject = row['xxxSubject']
  bankvorgang = row['Buchungstext']
      
  if betrag > 0
    # Spender
    if contact.nilastate_id == 2
      row['vorgang'] = 'Spende'

    # Foerderer
    elsif contact.nilastate_id == 3
      if subjectMatch(subject, %w(foerdermitglied mitgliedsbeitrag unterst erderbetrag rderbeitrag foerderbei forderbetrag))
        row['vorgang'] = 'Foerderbeitrag FD'
      elsif subjectMatch(subject, %w(patenschaft))
        row['vorgang'] = 'Patenschaftsbeitrag FD'
      elsif subjectMatch(bankvorgang, %w(dauerauftrag))
        row['vorgang'] = 'Foerderbeitrag FD'
      elsif subjectMatch(subject, %w(spende))
        row['vorgang'] = 'Spende FD'
      end

    # Aktives Mitglied
    elsif contact.nilastate_id == 4
      if subjectMatch(subject, %w(mitgliedsbeitrag))
        row['vorgang'] = 'Mitgliedsbeitrag AM'
      elsif subjectMatch(subject, %w(rderbeitrag foerderbei))
        row['vorgang'] = 'Foerderbeitrag AM'
      elsif subjectMatch(subject, %w(spende))
        row['vorgang'] = 'Spende AM'
      elsif subjectMatch(subject, %w(patenschaft))
        row['vorgang'] = 'Patenschaftsbeitrag AM'
      end

    # Foerdermitglied
    elsif contact.nilastate_id == 5
      if subjectMatch(subject, %w(mitgliedsbeitrag))
        row['vorgang'] = 'Mitgliedsbeitrag FM'
      elsif subjectMatch(subject, %w(mitgliedsbeitrag))
        row['vorgang'] = 'Mitgliedsbeitrag FM'
      elsif subjectMatch(subject, %w(patenschaft))
        row['vorgang'] = 'Patenschaftsbeitrag FM'
      elsif subjectMatch(bankvorgang, %w(dauerauftrag))
        row['vorgang'] = 'Mitgliedsbeitrag FM'
      elsif subjectMatch(subject, %w(spende))
        row['vorgang'] = 'Spende FM'
      end

    # Spender SE
    elsif contact.nilastate_id == 10
      row['vorgang'] = 'Spende SE'
    else
      if subjectMatch(subject, %w(amazon habenzinsen))
        row['vorgang'] = 'Ertrag'
      end
    end


  elsif betrag < 0
    if subjectMatch(subject, %w(verwaltunsaufwand))
      row['vorgang'] = 'Verwaltungsaufwand'
    end

  end

  return row


end

def createDataset(row)

  ai = Accountingimport.create!(
      blz_nila: 25120510,
      bankkonto_nila: 9495000,
      buchungsdatum: row['xxxBuchungsdatum'],
      betrag_eur: row['xxxBetrag'].to_f,
      bankvorgang: row['xxxBuchungstext'],
      blz_kontakt: row['BIC (SWIFT-Code) Zahlungsbeteiligter'],
      bankkonto_kontakt: row['IBAN Zahlungsbeteiligter'],
      name: row['xxxName'],
      bfs_subject: row['xxxSubject'],
      verwendungszweck: row['xxxSubject'],
      kontakt_nr: row['kontakt_nr'],
      vorgang: row['vorgang'],
      nila_konto_nr: -49002,
      nila_konto: 'NILA BfS',
      kontakt_nr_matching_count: row['kontakt_nr_matching_count']
  )
end

def usage(s)
  $stderr.puts(s)
  STDOUT.write "\n------- bfs_importer --------\n"
  STDOUT.write "      Usage:\n"
  STDOUT.write "      Please specify the file to import\n"
  STDOUT.write "          bfs_importer.rb [options] import.csv\n"
  STDOUT.write "      Options:\n"
  STDOUT.write "          -v     # verbose output (not yet implemented)\n"
  STDOUT.write "          -d     # column delimiter (not yet implemented)\n"
  STDOUT.write "      Example:\n"
  STDOUT.write "          bfs_importer.rb import.csv\n\n"
  exit(2)
end

$action = false
$importFile = nil

loop { 
    case ARGV[0]
      when '-q' then  ARGV.shift; $quiet = true
      when '-i' then  ARGV.shift; $importFile = ARGV.shift; $action = "import"
      when /^-/ then  usage("Unknown option: #{ARGV[0].inspect}")
      else break
    end; 
}

if ARGV.empty?
  usage("Error: No import file specified!\n")
  
else
  if File.exists?(ARGV[0])
    $staticContacts = cachedContacts()
    noMatches = Hash.new(0)
    multipleMatches = Hash.new(0)
    lines = CSV.read(ARGV[0], headers: true, col_sep: ';', encoding: 'UTF-8')

    lines.each_with_index do |row, index|
      preprocessedRow = preProcess(row)
      preprocessedRow['kontakt_nr'] = -1
      preprocessedRow['kontakt_nr_matching_count'] = 0

      contacts = determineKontakt(preprocessedRow)
      if contacts && contacts.size == 1
        contact = contacts.first

        if contact != nil
          preprocessedRow['kontakt_nr'] = contact.id
          preprocessedRow['kontakt_nr_matching_count'] = 1
          preprocessedRow = classifyVorgang(preprocessedRow, contact)
        end
      elsif contacts && contacts.size > 1
        # puts "Too many matching contacts found (amount: #{contacts.size})!"
	      contacts.each do |co|
	        puts "#{co.name}"
	      end
        preprocessedRow['kontakt_nr_matching_count'] = contacts.size
        multipleMatches[preprocessedRow['xxxName']] = contacts.size
      else
        noMatches[preprocessedRow['xxxName']] += 1
      end

      createDataset(preprocessedRow)
      percentReady = ((index.to_f / (lines.size - 1).to_f) * 100).to_i
      STDOUT.write "\rProgress: #{index} of #{lines.size - 1} (#{percentReady} %) imported"
    end

    STDOUT.write "\n\n"
    STDOUT.write "Summary:\n"
    STDOUT.write "No matches: #{noMatches.to_s}\n"
    STDOUT.write "Multiple matches: #{multipleMatches.to_s}\n"

  else
    STDERR.write "Error: File '#{ARGV[0]}' not found!\n"
  end
end
