require 'yaml'
require 'active_record'
require 'securerandom'
require 'logger'
require 'prawn'
require 'prawn/table'
require 'numbers_and_words'
require 'pp'

require_relative '../lib/nila_crm/contacts'
require_relative '../lib/nila_crm/helper'
require_relative '../lib/nila_crm/pdf'

params = {
  "no_data_overall"=>"",
  "kontaktinhaber"=>"",
  "id"=>"",
  "typus"=>"",
  "nilastate_id"=>"",
  "no_data_pate"=>"",
  "aktiv_inaktiv"=>"",
  "name"=>"",
  "vorname"=>"",
  "email_adresse"=>"",
  "no_data_geburtstag"=>"",
  "sprache"=>"",
  "versand_newsletter"=>"",
  "versand_aktuelles"=>"",
  "versand_kalender"=>"2",
  "beschreibung"=>"",
  "no_data_donations_last_from"=>"",
  "no_data_donations_last_to"=>"",
  "no_data_donations_betrag_brutto"=>"",
  "no_data_donations_vorgang"=>"",
  "no_data_writer"=>"Dein Florian Osterloh"
 }

logger   = Logger.new(STDOUT)
contacts = NilaCrm::Contacts.new(logger)
pdf      = NilaCrm::Pdf.new(logger, contacts)
writer   = params['no_data_writer']

result_contacts = contacts.find_contacts(params)

File.join('export/pdf', pdf.create_letter(result_contacts, 'calendar_letter', Date.today.year, writer, 10, 1))