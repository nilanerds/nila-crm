require 'sinatra'
require 'sinatra/multi_route'
require 'tilt/erb'
require 'yaml'
require 'active_record'
require 'securerandom'
require 'mail'
require 'logger'
require 'prawn'
require 'prawn/table'
require 'numbers_and_words'
require 'pp'

# Main Class
module NilaCrm
  require_relative 'nila_crm/contacts'
  require_relative 'nila_crm/csv'
  require_relative 'nila_crm/donations'
  require_relative 'nila_crm/helper'
  require_relative 'nila_crm/pdf'
  require_relative 'nila_crm/user'
  require_relative 'nila_crm/orphans'
  require_relative 'nila_crm/server'

  attr_writer :logger

  def logger
    unless @logger
      @logger = Logger.new(STDOUT)
      @logger.level = (ENV['LOG_LEVEL'] || Logger::INFO).to_i
    end
    @logger
  end

  def version
    unless @version
      begin
        @version = YAML.load_file('version.yaml')
      rescue Errno::ENOENT
        @version = { commit: nil, version: 'local', name: 'local' }
      end
    end
    @version
  end

  module_function :logger, :logger=, :version
end
