module NilaCrm
  # All pdf things
  class Csv
    FP = File.dirname(__FILE__) + '/../../public/export/csv/'

    def initialize(logger)
      @logger = logger
    end

    def export_contacts(contacts, years = nil, donations = nil)
      fn     = File.join(FP, "Contacts.csv")
      result = contacts.first.attributes.keys.join(';') + "\n"
      contacts.each do |contact|
        result += contact.attributes.values.join(';').gsub(/[\r\n]/,'') + "\n"
      end
      File.open(fn, 'w+') { |file| file.write(result) }
      File.basename(fn)
    end

    def export_contacts_with_donations(contacts, years, donations)
      fn     = File.join(FP, "ContactsWithDonations.csv")
      result = (%w(id name vorname status) << years << 'Summe').join(';') + "\n"
      contacts.each do |c|
        next unless donations[c.id]
        donation = donations[c.id].group_by { |d| d.buchungs_datum.year }
        name     = c.name || c.se_name
        state    = c.nilastate.name if c.nilastate
        tmp      = []
        years.each do |y|
          donation[y].nil? ? tmp << 0 : tmp << donation[y].sum { |d| d.betrag_brutto.to_f }
        end
        result += ([c.id, name, c.vorname, state] << tmp << tmp.sum.round(2)).join(';') + "\n"
      end
      File.open(fn, 'w+') { |file| file.write(result) }
      File.basename(fn)
    end

    def export_accounts(donations, accounts, contacts, year, stocks)
      fn     = File.join(FP, "Bestandskonten_#{year}.csv")
      result = ''

      accounts.each do |account|
        donation = donations.select { |d| d.kontonr_nila == account.id }

        next if donation.empty?
        stock = stocks[account.id][year]
        sum_eur = 0
        sum_mmk = 0

        result += "#{account.name}\n"
        result += 'Buchungsdatum,Name,Vorname,Betrag Euro,'
        result += 'Betrag MMK,Umrechnungskurs,Vorgang,Vorgang Pendant,Vorgang Art,AufwandsNr,'
        result += 'Rechnungsdatum,Verwendungszweck'
        result += "\n"

        donation.each do |d|
          contact = contacts.find { |a| a.id == d.contact_id }
          sum_eur += d.betrag_brutto.to_f
          sum_mmk += d.betrag_mmk.to_f

          result += "#{d.buchungs_datum},"
          result += "#{contact.name unless contact.nil?},"
          result += "#{contact.vorname unless contact.nil?},"
          result += "#{d.betrag_brutto},"
          result += "#{d.betrag_mmk},"
          result += "#{d.cr},"
          result += "#{d.vorgang},"
          result += "#{d.vorgang_kommentar},"
          result += "#{d.vorgang_art},"
          result += "#{d.aufwands_nummer},"
          result += "#{d.rechnungsdatum},"
          result += "#{d.verwendungszweck}"
          result += "\n"
        end
        result += "Summe,,,#{sum_eur.round(2)},#{sum_mmk.round(2)},,,,,,,\n"
        result += "Anfangsbestand,,,#{stock[:eur_start]},#{stock[:mmk_start]},,,,,,\n"
        result += "Endbestand,,,#{stock[:eur_end]},#{stock[:mmk_end]},,,,,,\n\n"
      end
      File.open(fn, 'w+') { |file| file.write(result) }
      File.basename(fn)
    end

    def export_donations(donations, year, kind)
      fn = File.join(FP, "#{kind}_#{year}.csv")
      result = ''

      donations.each do |name, d|
        next if d.empty?
        t_sum = 0
        result += "#{name}\n"
        result += ",1,2,3,4,5,6,7,8,9,10,11,12,Summe\n"
        d.each do |k, dk|
          k_sum = 0
          result += k
          dk.each do |_, sum|
            k_sum += sum
            result += ",#{sum.round(2)}"
          end
          result += ",#{k_sum.round(2)}\n"
          t_sum += k_sum
        end

        result += 'Summe'
        (1..12).each do |month|
          result += ",#{d.sum { |_, dm| dm[month] }.round(2)}"
        end
        result += ",#{t_sum.round(2)}"
        result += "\n\n\n"
      end
      File.open(fn, 'w+') { |file| file.write(result) }
      File.basename(fn)
    end

    def export_myanmar(donations, contacts, year)
      fn = File.join(FP, "Myanmar_#{year}.csv")
      result = ''
      donations.group_by(&:contact_id).each do |id, donation|
        sum_eur = 0
        sum_mmk = 0

        result += contacts.find { |c| c.id == id }.name + "\n"
        result += 'Nila KontoNr,Nila Konto,Buchungsdatum,KontaktNr,Betrag Euro,'
        result += 'Betrag MMK,Vorgang,Vorgang Pendant,Vorgang Art,AufwandsNr,'
        result += "Rechnungsdatum,Verwendungszweck\n"

        donation.group_by(&:vorgang).each do |_, vd|
          vd.each do |d|
            sum_eur += d.betrag_brutto.to_f
            sum_mmk += d.betrag_mmk.to_f

            result += "#{d.kontonr_nila},"
            result += "#{d.konto_nila},"
            result += "#{d.buchungs_datum},"
            result += "#{d.contact_id},"
            result += "#{d.betrag_brutto},"
            result += "#{d.betrag_mmk},"
            result += "#{d.vorgang},"
            result += "#{d.vorgang_kommentar},"
            result += "#{d.vorgang_art},"
            result += "#{d.aufwands_nummer},"
            result += "#{d.rechnungsdatum},"
            result += "#{d.verwendungszweck}"
            result += "\n"
          end
        end
        result += "Summe,,,,#{sum_eur.round(2)},#{sum_mmk.round(2)},,,,,\n\n"
      end
      File.open(fn, 'w+') { |file| file.write(result) }
      File.basename(fn)
    end
  end
end
