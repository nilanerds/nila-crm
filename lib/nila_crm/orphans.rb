require_relative 'db'

module NilaCrm
  # All Orphan stuff
  class Orphans
    include NilaCrm::Db

    def initialize(logger)
      @logger = logger
      @helper = NilaCrm::Helper
    end

    def del_orphan(id)
      Orphan.delete(id)
    end

    def add_orphan(params)
      Orphan.create(params)
    end

    def update_orphan(params)
      orphan = Orphan.find_by(id: params['id'])

      orphan.update(params.each { |k, v| params[k] = v.to_s.strip })
    end

    def check_orphan(params)
      name = params[:name].upcase.strip
      Orphan.where('UPPER(name) = ?', name).size
    end
  end
end
