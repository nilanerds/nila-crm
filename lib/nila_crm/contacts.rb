require_relative 'db'

module NilaCrm
  # All Contact stuff
  class Contacts
    include NilaCrm::Db

    def initialize(logger)
      @logger = logger
      @helper = NilaCrm::Helper
    end

    def find_contacts(params)
      result = []

      return result if params.empty?

      check = params.select { |k, v| k =~ /_donations_/ && !v.empty? }.empty?

      c = Contact.where(@helper.generate_sql(params))

      return @helper.check_donations(c, params) unless check

      c
    end

    def add_contact(params)
      Contact.create(params)
    end

    def update_contact(params, changer_id)
      contact = Contact.find_by(id: params['id'])

      contact_a = contact.as_json.map { |k, v| [k, v.to_s] }
      params_a  = params.map { |k, v| [k, v.to_s.strip] }

      diff = params_a - contact_a

      return true if diff.empty?

      add_contact_history(@helper.contact_history(contact, 'update', changer_id, diff))

      contact.update(params.each { |k, v| params[k] = v.to_s.strip })
    end

    def del_contact(id)
      Contact.delete(id)
    end

    def add_contact_history(params)
      Contacthistory.create(params.each { |k, v| params[k] = v.to_s.strip })
    end

    def check_contact(params)
      name    = params[:name].upcase.strip if params[:name]
      vorname = params[:vorname].upcase.strip if params[:vorname]
      se_name = params[:se_name].upcase.strip if params[:se_name]

      Contact.where('(UPPER(name) = ? AND UPPER(vorname) = ?)
                     OR UPPER(se_name) = ?', name, vorname, se_name).size
    end
  end
end
