require_relative 'db'
# NILA CRM
module NilaCrm
  # All user stuff
  class User
    include NilaCrm::Db

    def initialize(logger)
      @logger = logger
    end

    def check(user, pass)
      hashed_pass = Digest::SHA256.hexdigest pass.to_s
      Crmuser.all.find { |u| u.username == user && u.hashed_password == hashed_pass }
    end

    def login(user)
      login = Digest::SHA256.hexdigest user.username + user.hashed_password
      Crmuser.update(user.id, login: login)
      login
    end

    def logout(user_id)
      Crmuser.update(user_id, login: '')
    end

    def auth(login)
      return nil if login.nil?
      Crmuser.find_by(login: login)
    end

    def add_user(params)
      params['hashed_password'] = Digest::SHA256.hexdigest params['pwd']
      params['login'] = ''

      params.reject! { |k, _| k =~ /pwd/ }

      Crmuser.create(params).id
    end

    def update_user(user_id, params)
      params['hashed_password'] = Digest::SHA256.hexdigest params['pwd'] unless params['pwd'].to_s.empty?
      params.reject! { |k, v| k =~ /pwd/ || v.empty? }
      Crmuser.update(user_id, params)
    end

    def check_user(params)
      Crmuser.find_by(username: params['username'])
    end

    def del_user(user_id)
      Crmuser.delete(user_id)
    end

    def send_mail(user, hash)
      mail = Mail.new do
        from 'crm@nila-ev.de'
        to user.email.to_s
        subject 'Passowort zurücksetzen'
        body "https://crm.nila-ev.de/NewPass?hash=#{hash}"
      end

      mail.deliver
    end
  end
end
