require_relative 'db'
module NilaCrm
  # All the small things
  class Helper
    include NilaCrm::Db
    def self.years
      (2010..Date.today.strftime('%Y').to_i).to_a
    end

    def self.month
      %w(
        Januar
        Februar
        März
        April
        Mai
        Juni
        Juli
        August
        September
        Oktober
        November
        Dezember
      )
    end

    def self.titel
      ['Dipl. Ing.', 'Dr.', 'Prof. Dr.']
    end

    def self.active_inactive
      %w(Inaktiv Aktiv)
    end

    def self.gender
      %w(Junge Mädchen)
    end

    def self.family_status
      ['Poor Child', 'Halbwaise', 'Vollwaise']
    end

    def self.language
      %w(Deutsch Englisch)
    end

    def self.salutation
      %w(Herr Frau)
    end

    def self.orphan_salutation
      %w(Ma Maung)
    end

    def self.se_kind
      %w(Club Sonstiges Stiftung Unternehmen)
    end

    def self.generate_sql(params)
      sql = ''
      sql += '(' + overall(params['no_data_overall']) + ') and ' unless params['no_data_overall'].empty?
      sql += '(1=1'
      sql += " and MONTH(geburtstag)=#{params['no_data_geburtstag'].to_i}" unless params['no_data_geburtstag'].to_s.empty?
      sql += ' and orphan_id > 0' if params['no_data_pate'].to_i == 1
      sql += ' and (orphan_id < 0 or orphan_id = NULL)' if params['no_data_pate'].to_i == 2

      params.each do |k, v|
        next if v.empty? || k =~ /no_data_/
        ops = "#{k} like '%#{v}%'"
        ops = "#{k}=#{v}" unless v.to_i.zero?

        ops = "(name like '%#{v}%' or se_name like '%#{v}%' or geburtsname like '%#{v}%')" if k == 'name'
        ops = "#{k} = '#{v}'" if k == 'aktiv_inaktiv'
        ops = "(email_adresse like '%#{v}%' or se_email like '%#{v}%')" if k == 'email_address'

        sql += " and #{ops}"
      end
      sql += ')'
      sql
    end

    def self.overall(search)
      sql  = []
      cols = %w(name vorname se_name geburtsname email_adresse id beschreibung se_email)
      cols.each do |col|
        ops = "#{col} like '%#{search}%'"
        ops = "#{col}=#{search}" unless search.to_i.zero?
        sql << ops
      end
      sql.join(' or ')
    end

    def self.check_donations(contacts, params)
      result = []
      donations = Unionaccounting.all.group_by(&:contact_id)

      from = Date.parse(from_d(params['no_data_donations_last_from']))
      to = Date.parse(to_d(params['no_data_donations_last_to']))
      betrag_brutto = params['no_data_donations_betrag_brutto'].to_f
      betrag_brutto = '.*' if betrag_brutto == 0
      vorgang = params['no_data_donations_vorgang'].presence || '.*'

      contacts.each do |c|
        next if donations[c.id].nil?
        d = []
        donations[c.id].each do |a|
          next unless a.buchungs_datum >= from \
                && a.buchungs_datum <= to \
                && a.betrag_brutto.to_s =~ /^#{betrag_brutto.to_s}$/ \
                && a.vorgang =~ /#{vorgang}/
          d << a
        end
        result << c if d.size > 0
      end
      result
    end

    def self.from_d(date)
      date = date.to_s.presence || '0000-01'
      date + '-01'
    end

    def self.to_d(date)
      date = date.to_s.presence || '9999-12'
      date + '-31'
    end

    def self.get_contact_changes(contact, diffs)
      message = ''
      diffs.each do |diff|
        key, value = diff
        message += "Der Wert '#{key}' wurde von '#{contact[key]}' auf '#{value}' geändert \n"
      end
      message
    end

    def self.contact_history(contact, typus, changer_id, message)
      message = get_contact_changes(contact, message) if typus == 'update'
      change = {}
      change[:contact_id] = contact.id
      change[:typus] = typus
      change[:message] = message
      change[:timestamp] = Time.now
      change[:changer_id] = changer_id
      change
    end

    def self.conversion_rate(donations, year)
      donations = donations.select do |d|
        d.betrag_mmk.to_f > 0 && d.betrag_brutto.to_f > 0 &&
          d.contact_id == -698 && d.buchungs_datum.year == year
      end

      anfang = Anfangsbestand.find { |a| a.year == year }
      anfang_eur = anfang.betrag_eur unless anfang.nil?
      anfang_mmk = anfang.betrag_mmk unless anfang.nil?

      sum_eur = donations.sum { |d| d.betrag_brutto.to_f } + anfang_eur.to_f
      sum_mmk = donations.sum { |d| d.betrag_mmk.to_f } + anfang_mmk.to_f

      cr = sum_mmk / sum_eur

      cr.round(2)
    end

    def self.file_name(contact, year)
      fn       = ''
      lastname = contact.name.presence || contact.se_name || ''
      name     = contact.vorname || ''

      ln = lastname.delete('/').tr(' ', '_')
      n  = name.delete('/').tr(' ', '_')

      fn =  ln + '_' unless ln.empty?
      fn += n + '_' unless n.empty?

      fn + year.to_s + '.pdf'
    end

    def self.new_line(count = 1)
      result = ''
      (1..count).each do
        result += "\n"
      end
      result
    end

    def self.clean_up_export_path(fp, ext = '*')
      FileUtils.rm_rf Dir.glob(File.join(fp, "*.#{ext}"))
    end

    def self.zip_files(fp, kind, year)
      zip_file = "#{kind}_#{year}.zip"

      `cd #{fp}; zip #{zip_file} *.pdf`

      clean_up_export_path(fp, 'pdf')

      zip_file
    end

    def self.random_hash
      SecureRandom.hex
    end

    def self.speech(alternatives, speech)
      alternatives[speech]
    end
  end
end
