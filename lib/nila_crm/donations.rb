require_relative 'db'

module NilaCrm
  # All Contact stuff
  class Donations
    include NilaCrm::Db
    def initialize(logger)
      @logger = logger
      @helper = NilaCrm::Helper
    end

    def get_donations(year = nil)
      donations = Unionaccounting.all
      donations = donations.select { |d| !d.buchungs_datum.nil? && d.buchungs_datum.year == year } unless year.nil?
      convert_mmk(donations)
    end

    def stock
      result    = {}
      donations = get_donations

      Account.pluck(:id).each do |id|
        result[id] = {}
        account_donations = donations.select { |d| d.kontonr_nila == id }
        @helper.years.each do |year|
          start_mmk_sum = start_eur_sum = mmk_sum = eur_sum = 0

          account_donations.select { |d| d.buchungs_datum.year == year.to_i }.each do |d|
            mmk_sum += d.betrag_mmk.to_f
            eur_sum += d.betrag_brutto.to_f
          end

          account_donations.select { |d| d.buchungs_datum.year < year.to_i }.each do |d|
            start_mmk_sum += d.betrag_mmk.to_f
            start_eur_sum += d.betrag_brutto.to_f
          end

          result[id][year] = {
            mmk_start: start_mmk_sum.round(2),
            mmk_end:   (start_mmk_sum + mmk_sum).round(2),
            eur_start: start_eur_sum.round(2),
            eur_end:   (start_eur_sum + eur_sum).round(2)
          }
        end
      end
      result
    end

    def evaluation_data(donation_kind = nil, year = nil)
      result    = {}
      donations = get_donations(year)
      states    = nila_states
      year.nil? ? counter = @helper.years : counter = (1..12).to_a

      states.select! { |s| s.buchfuehrung == donation_kind } unless donation_kind.nil?

      states.each do |s|
        result[s.name] = {}
        vorgaenge      = s.kontakt.to_s.split(',').presence ||
                         s.vorgang.to_s.split(',').presence ||
                         s.nila_konto.to_s.split(',').presence

        vorgaenge.each do |v|
          if s.kontakt || s.nila_konto
            begin
              name = Contact.where(id: v).first.name
            rescue
              name = "ERROR: Es gibt keinen Eintrag in der contacts-Tabelle mit der id #{v}"
            end
          end

          name = v if s.vorgang

          result[s.name][name] = {}

          vorgang_donations = donations.select do |d|
            v == d.vorgang || v == d.vorgang_kommentar ||
              v.to_i == d.contact_id ||
              v.to_i == d.kontonr_nila
          end

          counter.each do |c|
            result[s.name][name][c] = {}
            vd = vorgang_donations.select { |d| d.buchungs_datum.year == c || d.buchungs_datum.month == c }
            result[s.name][name][c] = vd.sum { |d| d.betrag_brutto.to_f }.round(2)
          end
        end
      end

      result
    end

    private

    def convert_mmk(donations)
      result = []
      donations.group_by { |d| d.buchungs_datum.year }.each do |year, donation|
        cr = @helper.conversion_rate(donation, year)

        donation.each do |d|
          if d.betrag_mmk.to_i != 0 && d.betrag_brutto.to_i.zero? && !cr.nan? && !cr.zero?
            d.betrag_brutto = (d.betrag_mmk.to_f / cr)
            d.cr = cr
          end
          result << d
        end
      end
      result
    end

    def nila_states
      Nilastate.all.select { |n| n.vorgang || n.kontakt || n.nila_konto }
    end
  end
end
