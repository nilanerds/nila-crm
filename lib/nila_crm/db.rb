# NILA CRM
module NilaCrm
  # DB-Stuff
  module Db
    begin
      db_conf = File.join(File.dirname(__FILE__), '../../config/db.yaml')
      ActiveRecord::Base.establish_connection(YAML.load_file(db_conf))
    rescue => e
      puts e.message
    end

    # Contact Class
    class Contact < ActiveRecord::Base
      ActiveRecord::Base.transaction do
        has_many :unionaccountings
        has_many :contacthistories
        has_many :orphans
        belongs_to :nilastate
      end
    end

    # Orphan Class
    class Orphan < ActiveRecord::Base
      belongs_to :contact
    end

    # Contacthistory Class
    class Contacthistory < ActiveRecord::Base
      belongs_to :contact
    end

    # Nilastate Class
    class Nilastate < ActiveRecord::Base
      has_one :contact
    end

    # Section Class
    class Section < ActiveRecord::Base
      has_many :positions
    end

    # Postion Class
    class Position < ActiveRecord::Base
      belongs_to :section
    end

    # Contacthistory Class
    class Contacthistory < ActiveRecord::Base
      belongs_to :contact
    end

    # Unionaccounting Class
    class Unionaccounting < ActiveRecord::Base
      belongs_to :contact
    end

    # Crmuser Class
    class Crmuser < ActiveRecord::Base
    end

    # Nilatype Class
    class Nilatype < ActiveRecord::Base
    end

    # Anfangsbestand Class
    class Anfangsbestand < ActiveRecord::Base
    end

    # Distributionstate Class
    class Distributionstate < ActiveRecord::Base
    end

    # Contactowner Class
    class Contactowner < ActiveRecord::Base
    end

    # Procedure Class
    class Procedure < ActiveRecord::Base
    end

    # Countrie Class
    class Countrie < ActiveRecord::Base
    end

    # Socialkind Class
    class Socialkind < ActiveRecord::Base
    end

    # ActiveMember Class
    class Activemember < ActiveRecord::Base
    end

    # Accounts Class
    class Account < ActiveRecord::Base
    end

    # Accounts Class
    class Accountingimport < ActiveRecord::Base
    end
    #
    # AccountingImportsAssignment Class
    class Accountingimportsassignment < ActiveRecord::Base
    end
  end
end
