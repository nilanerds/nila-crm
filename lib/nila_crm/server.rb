module NilaCrm
  # THE sinatra base class.
  class Server < Sinatra::Base
    register Sinatra::MultiRoute
    enable :sessions

    require_relative 'db'

    include NilaCrm::Db

    def logger
      NilaCrm.logger
    end

    def initialize
      super
      @contacts  = NilaCrm::Contacts.new(logger)
      @csv       = NilaCrm::Csv.new(logger)
      @donations = NilaCrm::Donations.new(logger)
      @pdf       = NilaCrm::Pdf.new(logger, @contacts)
      @user      = NilaCrm::User.new(logger)
      @orphans   = NilaCrm::Orphans.new(logger)
      @helper    = NilaCrm::Helper
    end

    configure do
      set :views, File.dirname(__FILE__) + '/../../views'
      set :public_dir, File.dirname(__FILE__) + '/../../public'
    end

    helpers do
      def authorize!
        # request.path_info += '?' + request.query_string unless request.query_string.empty?
        session[:path] = request.path_info
        redirect '/Login' if @user.auth(session[:id]).nil?
      end
    end

    get '/status' do
      'OK'
    end

    route :get, :post, ['/', '/Login'] do
      @login_error = false
      user = @user.check(params[:username], params[:password])
      if user
        session[:user_id] = user.id
        session[:id] = @user.login user
        session[:permisson] = user.permission_level

        redirect '/Home'
      elsif request.post?
        @login_error = true
      end

      erb :Login
    end

    get '/Logout' do
      @user.logout session[:user_id]
      redirect '/Login'
    end

    post '/LostPass' do
      @crm_user = @user.check_user(params)
      hash = @helper.random_hash

      @user.update_user(@crm_user.id, login: hash) if @crm_user
      @user.send_mail(@crm_user, hash) if @crm_user

      erb :LostPass
    end

    get '/NewPass' do
      @crm_user = @user.auth(params['hash'])

      erb :NewPass
    end

    post '/ResetPass' do
      crm_user = @user.auth(params['hash'])
      redirect '/Login' if @user.update_user(crm_user.id, 'pwd1' => params['pwd1'], 'login' => '0')
    end

    get '/Home' do
      authorize!

      erb :default
    end

    get '/Contacts' do
      authorize!

      @months             = @helper.month
      @years              = @helper.years
      @active_inactive    = @helper.active_inactive
      @language           = @helper.language
      @typus              = Nilatype.all
      @states             = Nilastate.all
      @contactowners      = Contactowner.all
      @procedures         = Procedure.all
      @distributionstates = Distributionstate.all
      @contacts           = {}
      @donations          = {}

      erb :default
    end

    post '/Contacts' do
      authorize!

      @donations = @donations.get_donations.group_by(&:contact_id)
      @contacts  = @contacts.find_contacts(params)
      @years     = @helper.years

      erb :_contacts_result
    end

    get '/ContactInfo' do
      authorize!

      @sections  = Section.order('pos')
      @contact   = Contact.find_by id: params['id']
      @donations = @donations.get_donations.group_by(&:contact_id)
      @crm_user  = Crmuser.all

      erb :Contactinfo
    end

    route :get, ['/AddContact', '/EditContact'] do
      authorize!

      id                  = params[:id] || -666
      @months             = @helper.month
      @active_inactive    = @helper.active_inactive
      @language           = @helper.language
      @salutation         = @helper.salutation
      @titel              = @helper.titel
      @se_kind            = @helper.se_kind
      @sections           = Section.order('pos')
      @contact            = Contact.where(id: id).first
      @states             = Nilastate.all
      @contactowners      = Activemember.all
      @countries          = Countrie.all
      @distributionstates = Distributionstate.where.not(name: '')
      @socialkinds        = Socialkind.all
      @orphans            = Orphan.all

      if request.path_info =~ /Edit/
        erb :EditContact
      else
        erb :default
      end
    end

    post '/CheckContact' do
      count = @contacts.check_contact params

      count.to_s
    end

    post '/AddContact' do
      authorize!
      return 401 unless session[:permisson] == 'rw'
      @contacts.add_contact(params)

      Contact.maximum(:id).to_s
    end

    post '/UpdateContact' do
      authorize!

      @contacts.update_contact(params, session[:user_id])
    end

    post '/DelContact' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      return 200 if @contacts.del_contact params[:id]
    end

    get '/Orphans' do
      authorize!

      @orphans = Orphan.all

      erb :default
    end

    route :get, ['/AddOrphan', '/EditOrphan'] do
      authorize!

      id                 = params[:id] || -666
      @active_inactive   = @helper.active_inactive
      @gender            = @helper.gender
      @family_status     = @helper.family_status
      @orphan_salutation = @helper.orphan_salutation
      @countries         = Countrie.all
      @orphan            = Orphan.find_by id: id
      @sections          = Section.order('pos')
      @orphanges         = Contact.where(typus: 'Waisenhaus')

      if request.path_info =~ /Edit/
        erb :EditOrphan
      else
        erb :default
      end
    end

    get '/OrphanInfo' do
      authorize!

      @sections = Section.order('pos')
      @orphan   = Orphan.find_by id: params[:id]
      @sections = Section.order('pos')

      erb :Orphaninfo
    end

    post '/CheckOrphan' do
      @orphans.check_orphan(params).to_s
    end

    post '/AddOrphan' do
      authorize!

      @orphans.add_orphan(params)

      Orphan.maximum(:id).to_s
    end

    post '/UpdateOrphan' do
      authorize!

      @orphans.update_orphan(params)
    end

    post '/DelOrphan' do
      authorize!

      return 401 unless session[:permisson] == 'rw'

      return 200 if @orphans.del_orphan params[:id]
    end

    get '/Mail' do
      authorize!

      @contacts = @contacts.find_contacts(params)

      erb :Mail
    end

    post '/ExportContact' do
      authorize!

      years     = @helper.years
      contacts  = @contacts.find_contacts(params)
      donations = @donations.get_donations.group_by(&:contact_id)

      file = File.join('export/csv', @csv.send(params[:no_data_method], contacts, years, donations))

      file.to_s if File.exist?(File.join('public', file))
    end

    post '/DonationReceipt' do
      authorize!

      contacts  = @contacts.find_contacts(params)
      year      = params[:no_data_donation_year]
      donations = @donations.get_donations(year.to_i).group_by(&:contact_id)

      file = File.join('export/pdf', @pdf.create_letter(contacts, 'donation_receipt', year, donations, 10, session[:user_id]))

      file.to_s if File.exist?(File.join('public', file))
    end

    post '/CalendarLetter' do
      authorize!

      contacts = @contacts.find_contacts(params)
      writer   = params[:no_data_writer]

      file = File.join('export/pdf', @pdf.create_letter(contacts, 'calendar_letter', Date.today.year, writer, 10, session[:user_id]))

      file.to_s if File.exist?(File.join('public', file))
    end

    get '/Donations' do
      authorize!

      @kind      = params[:kind]
      @donations = @donations.evaluation_data(@kind)
      @contacts  = Contact.all
      @years     = @helper.years

      erb :default
    end

    post '/DonationEvaluation' do
      authorize!

      year        = params['year'].to_i
      export_kind = params['export_kind']
      kind        = params['kind']
      donations   = @donations.evaluation_data(kind, year)

      export_kind == 'csv' ? export = @csv : export = @pdf

      file = export.export_donations(donations, year, kind)

      "export/#{export_kind}/#{file}"
    end

    route :get, :post, '/Accounts' do
      authorize!

      @years     = @helper.years
      @donations = @donations.get_donations(params['year'].to_i).select { |t| t.kontonr_nila == params['account'].to_i }
      @contacts  = Contact.all
      @accounts  = @contacts.select { |c| Account.pluck(:id).include? c.id }

      erb :default
    end

    post '/ExportAccounts' do
      authorize!

      year        = params['year'].to_i
      export_kind = params['kind']
      donations   = @donations.get_donations year
      contacts    = Contact.all
      accounts    = contacts.select { |c| Account.pluck(:id).include? c.id }.sort_by(&:id).reverse

      export_kind == 'csv' ? export = @csv : export = @pdf

      file = export.export_accounts(donations, accounts, contacts, year, @donations.stock)

      "export/#{export_kind}/#{file}"
    end

    route :get, :post, '/Myanmar' do
      year       = params['year'] || 666
      @donations = @donations.get_donations(year.to_i).reject { |t| t.betrag_mmk.to_i.zero? }
      @contacts  = Contact.all
      @years     = @helper.years

      erb :default
    end

    post '/ExportMyanmar' do
      authorize!

      year        = params['year']
      export_kind = params['kind']
      donations   = @donations.get_donations(year.to_i).reject { |t| t.betrag_mmk.to_i.zero? }
      contacts    = Contact.all

      export_kind == 'csv' ? export = @csv : export = @pdf

      file = export.export_myanmar(donations, contacts, year)

      "export/#{export_kind}/#{file}"
    end

    get '/Users' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      @crm_user = Crmuser.all

      erb :default
    end

    get '/AddUser' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      @crm_user = Crmuser.find_by(id: params[:id])

      erb :default
    end

    get '/EditUser' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      @crm_user = Crmuser.find_by(id: params[:id])

      erb :EditUser
    end

    post '/UpdateUser' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      @user.update_user(params['id'], params)
    end

    post '/AddUser' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      id = @user.add_user(params)

      id.to_s
    end

    post '/CheckUser' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      id = @user.check_user(params) || 0

      id.to_s
    end

    post '/DelUser' do
      authorize!
      return 401 unless session[:permisson] == 'rw'

      200 if @user.del_user(params['id'])
    end

    get '/Sven' do
      authorize!

      erb :default
    end
  end
end
