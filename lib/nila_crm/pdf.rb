module NilaCrm
  # All pdf things
  class Pdf
    # Prawn::Font::AFM.hide_m17n_warning = true
    FP   = File.dirname(__FILE__) + '/../../public/export/pdf/'
    LOGO = File.dirname(__FILE__) + '/../../public/nila_logo.png'

    def initialize(logger, contacts)
      @logger   = logger
      @helper   = NilaCrm::Helper
      @contacts = contacts
    end

    def create_letter(contacts, kind, year = Date.today.year, donations = {}, font = 11, user_id)
      @helper.clean_up_export_path FP

      contacts.each do |c|
        next if c.id < 0
        next if donations[c.id].nil? && kind == 'donation_receipt'

        donations[c.id].select! { |d| !d.vorgang.to_s.include?('Storno') } if !donations[c.id].nil? && kind != 'calendar_letter'

        next if donations[c.id].to_s.empty? && kind == 'donation_receipt'

        if c.se_name.presence
          name      = c.se_name
          a_partner = [c.se_ap_anrede, c.se_ap_vorname, c.se_ap_name].join(' ')
          anrede    = nil
        else
          name      = [c.titel, c.vorname, c.name].join(' ')
          anrede    = c.anrede
          a_partner = nil
        end

        @logger.info "Generate #{kind} for #{name}"

        plz     = c.plz.presence || c.plz_geschaeftlich
        stadt   = c.stadt.presence || c.stadt_geschaeftlich
        strasse = c.strasse.presence || c.strasse_geschaeftlich
        land    = c.land.presence || c.land_geschaeftlich

        fn = File.join(FP, @helper.file_name(c, year))

        Prawn::Document.generate(fn, page_size: 'A4', right_margin: 51, left_margin: 49) do |pdf|
          pdf.font 'Helvetica'
          pdf.font_size 7

          pdf.repeat :all do
            pdf.image LOGO, at: [380, 785], scale: 0.13
            footer pdf
          end

          adress pdf

          pdf.font_size font

          pdf.bounding_box([23, 650], width: 200, height: 75) do
            pdf.text anrede if anrede
            pdf.text name
            pdf.text a_partner.strip unless a_partner.to_s.empty?
            pdf.text strasse
            pdf.text [plz, stadt].join(' ')
            pdf.text land
          end

          send kind, c, name, donations, year, pdf, user_id
        end
      end
      @helper.zip_files(FP, kind, year)
    end

    def adress(pdf)
      pdf.bounding_box([10, 670], width: 200, height: 75) do
        pdf.text 'Nila e.V. · Am Langenacker 25 · 53343 Wachtberg'
      end
    end

    def footer(pdf)
      pdf.bounding_box([0, 15], width: 200, height: 400) do
        pdf.text 'Vorstand Nila e.V.:
                  Ralf Müller, Monica Cocco', align: :left, color: 'b9785c'
      end
      pdf.bounding_box([120, 15], width: 200, height: 400) do
        pdf.text 'Vereinsregister Nr. VR 29369 B
                  Amtsgericht Charlottenburg | Berlin', align: :left, color: 'b9785c'
      end
      pdf.bounding_box([250, 15], width: 200, height: 400) do
        pdf.text 'Finanzamt für Körperschaften | Berlin
                  Steuernummer: 27 / 673 / 53826', align: :left, color: 'b9785c'
      end
      pdf.bounding_box([385, 15], width: 200, height: 400) do
        pdf.text 'Bank für Sozialwirtschaft
                  IBAN: DE75 25120510 0009495000
                  BIC: BFS WDE 33HAN', align: :left, color: 'b9785c'
      end
    end

    def calendar_letter(contact, _, writer, _, pdf, _)
      date = Time.new

      pdf.bounding_box([405, 600], width: 200, height: 75) do
        pdf.text "#{date.day}. #{@helper.month[date.month.to_i - 1]} #{date.year}", size: 11
      end

      pdf.text 'NILA Kalender 2021', leading: 2.5, align: :justify, size: 11, style: :bold

      pdf.text @helper.new_line 7
      pdf.text @helper.new_line

      pdf.text "zunächst möchten wir die Gelegenheit nutzen und uns ganz herzlich für Deine Unterstützung in 2020\
      bedanken. Auch in diesem Jahr sind wir wieder sehr beeindruckt von der vielfältigen Unterstützung, die NILA\
      e.V. und damit den Kindern in unseren Waisenhäusern zuteil wurde. ", leading: 2.5, align: :justify

      pdf.text @helper.new_line

      pdf.text "2020 hat uns alle in unterschiedlicher Weise von der Pandemie betroffen und unsere Gewohnheiten sowie\
      das persönliche Miteinander stark beeinflusst. Natürlich trifft der Shutdown auch Myanmar und damit unsere\
      Waisenhäuser hart, da u.a. der ausbleibende Tourismus Jobverlust für viele Menschen und das Ausbleiben\
      zahlreicher Spenden bedeutet. Die gute Nachricht ist aber, dass uns aktuell keine Fälle von COVID19\
      Infektionen in unseren Waisenhäusern bekannt sind. Wo es möglich ist, leben die meisten Kinder derzeit bei\
      ihren Familien und Verwandten.", leading: 2.5, align: :justify

      pdf.text @helper.new_line

      pdf.text "Auch wenn in diesem besonderen Jahr sehr viel aus unserem gewohnten Alltag und persönlichen\
      Beziehungen in virtuellen und digitalen Formen stattgefunden hat, hoffen wir, Dir mit unserem traditionellen\
      „analogen“ NILA Kalender 2021 wieder eine kleine Freude zu bereiten als Dankeschön für die Unterstützung.", leading: 2.5, align: :justify

      pdf.text @helper.new_line

      pdf.text "Entsprechend unserer Satzung werden unverändert alle Verwaltungskosten und damit auch die Kosten im\
      Zusammenhang mit dem Druck sowie Versand des NILA Kalender 2021 von den aktiven Mitgliedern getragen.\
      Alle Spenden kommen weiterhin vollständig den Projekten in Myanmar zu Gute.", leading: 2.5, align: :justify

      pdf.text @helper.new_line

      pdf.text "Das gesamte NILA-Team wünscht Dir schöne Weihnachtstage, Zeit für einen ruhigen Rückblick auf 2020\
      und insbesondere einen gesunden Start ins neue Jahr.", leading: 2.5, align: :justify


      pdf.text @helper.new_line

      pdf.text @helper.new_line
      pdf.text 'Herzliche Grüße'
      pdf.text @helper.new_line 4

      writer = writer.sub(/Ihr.{0,1} /, '') if contact.anrede_typ == 1

      pdf.text writer, leading: 2.5

      pdf.text 'NILA Spenderbetreuung', leading: 2.5 if writer !~ /Alexander/ && writer !~ /Rebecca/ && writer !~ /Ralf/
      pdf.text 'NILA Vorstand', leading: 2.5 if writer =~ /Ralf/
    end

    def donation_receipt(contact, name, donations, year, pdf, user_id)
      amount  = donations[contact.id].sum { |d| d.betrag_brutto }.round(2)
      word    = I18n.with_locale(:de) { amount.to_i.to_words }
      amount  = '%.2f' % amount
      c_date  = Time.new
      strasse = contact.strasse.presence || contact.strasse_geschaeftlich
      plz     = contact.plz.presence || contact.plz_geschaeftlich
      stadt   = contact.stadt.presence || contact.stadt_geschaeftlich

      pdf.text @helper.new_line 3

      if donations[contact.id].size > 1
        pdf.text 'Sammelbestätigung', size: 11, align: :center, style: :bold, color: 'b9785c'
      else
        pdf.text @helper.new_line
        pdf.text 'Bestätigung', size: 11, align: :center, style: :bold, color: 'b9785c'
      end

      pdf.text @helper.new_line
      pdf.text "über Geldzuwendungen im Sinne des § 10b des Einkommensteuergesetzes an eine der in § 5 Abs. 1 Nr. 9 des\
      Körperschaftsteuergesetzes bezeichneten Körperschaften, Personenvereinigungen oder Vermögensmassen.", align: :justify
      pdf.text @helper.new_line
      pdf.text 'Name und Anschrift des Zuwendenden:'
      pdf.text @helper.new_line
      pdf.text "#{name}, #{strasse}, #{plz} #{stadt}", align: :center, style: :bold, color: 'b9785c'
      pdf.text @helper.new_line
      pdf.text 'Betrag der Zuwendung in Ziffern - in Buchstaben - Tag der Zuwendung:'
      pdf.text @helper.new_line

      if donations[contact.id].size > 1
        pdf.text "EUR #{amount.tr('.', ',')} – #{word} – vom 01.01 bis 31.12 gem. Anlage", align: :center, style: :bold, color: 'b9785c'

      else
        date = donations[contact.id].first['buchungs_datum']
        pdf.text "EUR #{amount.tr('.', ',')} – #{word} – vom #{date.day}.#{date.month}.#{date.year}", align: :center, style: :bold, color: 'b9785c'
        pdf.text @helper.new_line
        pdf.text 'Es handelt sich um den Verzicht auf Erstattung von Aufwendungen: Nein'
      end

      pdf.text @helper.new_line

      pdf.text "Wir sind wegen Förderung der Entwicklungszusammenarbeit (§ 52 Abs. 2 Satz 1 Nr. 15 AO) nach dem letzten\
      uns zugegangenen Freistellungsbescheid bzw. nach der Anlage zum Körperschaftsteuerbescheid des\
      Finanzamtes für Körperschaften I Berlin, Steuernummer 27/673/53826, vom 14.07.2014 für den letzten\
      Veranlagungszeitraum 2011 - 2013 nach § 5 Abs. 1 Nr. 9 des Körperschaftsteuergesetzes von der\
      Körperschaftsteuer und nach § 3 Nr. 6 des Gewerbesteuergesetzes von der Gewerbesteuer befreit.", align: :justify
      pdf.text @helper.new_line
      pdf.text "Es wird bestätigt, dass die Zuwendung nur zur Förderung der Entwicklungszusammenarbeit (§ 52 Abs. 2 Satz 1\
      Nr. 15 AO) verwendet wird.", align: :justify

      if donations[contact.id].size > 1
        pdf.text @helper.new_line
        pdf.text "Es wird bestätigt, dass über die in der Gesamtsumme enthaltenen Zuwendungen keine weiteren Bestätigungen,\
        weder formelle Zuwendungsbestätigungen noch Beitragsquittungen oder ähnliches ausgestellt wurden und\
        werden. ", align: :justify
        pdf.text @helper.new_line
        pdf.text "Ob es sich um den Verzicht auf Erstattung von Aufwendungen handelt, ist der Anlage zur Sammelbestätigung\
        zu entnehmen.", align: :justify
      end

      pdf.text @helper.new_line
      pdf.text "Hamburg, den #{c_date.day}. #{@helper.month[c_date.month - 1]} #{c_date.year}"
      pdf.text @helper.new_line 5

      pdf.text @helper.new_line 3 if donations[contact.id].size == 1

      pdf.text 'Hinweis:', size: 8, style: :bold
      pdf.text @helper.new_line
      pdf.text "Wer vorsätzlich oder grob fahrlässig eine unrichtige Zuwendungsbestätigung erstellt oder wer veranlasst, dass Zuwendungen nicht zu den\
      in der Zuwendungsbestätigung angegebenen steuerbegünstigten Zwecken verwendet werden, haftet für die entgangene Steuer (§ 10b\
      Abs. 4 EStG, § 9 Abs. 3 KStG, § 9 Nr. 5 GewStG)." , size: 8, align: :justify
      pdf.text @helper.new_line
      pdf.text "Diese Bestätigung wird nicht als Nachweis für die steuerliche Berücksichtigung der Zuwendung anerkannt, wenn das Datum des\
      Freistellungsbescheides länger als 5 Jahre bzw. das Datum der Feststellung der Einhaltung der satzungsmäßigen Voraussetzungen nach\
      § 60a Abs. 1 AO länger als 3 Jahre seit Ausstellung des Bescheides zurückliegt (§ 63 Abs. 5 AO).", size: 8, align: :justify

      ch = @helper.contact_history(contact, 'generate', user_id, "Spendenbescheinigung über #{amount} € für das Jahr #{year} generiert")
      @contacts.add_contact_history(ch)

      return true if donations[contact.id].size == 1

      pdf.start_new_page

      pdf.bounding_box([10, 670], width: 250, height: 75) do
        pdf.text 'Anlage zur Sammelbestätigung', style: :bold, color: 'b9785c'
        pdf.text @helper.new_line
        pdf.text "von #{name} für das Jahr #{year}", style: :bold, color: 'b9785c'
      end

      pdf.text @helper.new_line 2

      table_data = [['Betrag']]
      tab = Prawn::Table.new(table_data,
        pdf,
        position: :center, column_widths: [55], cell_style: { border_width: 0, align: :right })

      table_data = [['Datum', 'Art der Zuwendung', 'Verzicht auf die Erstattung von Aufwendungen', tab]]
      tab = Prawn::Table.new(table_data,
        pdf,
        position: :center, column_widths: [110, 110, 140, 100], cell_style: { border_width: 0.5, borders: [:bottom], align: :center })
      tab.draw

      count = 1

      donations[contact.id].each do |donation|
        date = donation.buchungs_datum

        betrag = "%.2f" % donation.betrag_brutto
        month = '%02i' % date.month

        table_data = [["€ #{betrag.to_s.gsub(/\./, ',')}"]]
        tab = Prawn::Table.new(table_data,
          pdf,
          position: :center, column_widths: [55], cell_style: { border_width: 0, align: :right })

        table_data = [["#{date.day}.#{month}.#{date.year}", 'Geldspende', 'nein', tab]]
        tab = Prawn::Table.new(table_data,
          pdf,
          position: :center, column_widths: [110, 110, 140, 100], cell_style: { border_width: 0, align: :center })

        tab.draw

        if count % 20 == 0
          pdf.start_new_page
          pdf.bounding_box([10, 670], width: 250, height: 35) do
            pdf.text @helper.new_line
          end
        end
        count += 1
      end

      table_data = [["€ #{amount.tr('.',',')}"]]
      tab = Prawn::Table.new(table_data,
        pdf,
        position: :center, column_widths: [55], cell_style: { border_width: 0, align: :right, font_style: :bold, text_color: 'b9785c' })

      table_data = [['', '', 'Gesamtbetrag:', tab]]
      tab = Prawn::Table.new(table_data,
        pdf,
        position: :center, column_widths: [110, 110, 140, 100], cell_style: {
          border_width: 0.5, borders: [:top], align: :center, font_style: :bold, text_color: 'b9785c"'
        })

      tab.draw
    end

    def export_accounts(donations, accounts, contacts, year, stocks)
      fn = File.join(FP, "Bestandskonten_#{year}.pdf")

      Prawn::Document.generate(fn, page_size: 'A4', right_margin: 51, left_margin: 49) do |pdf|
        pdf.font 'Helvetica'
        pdf.font_size 11

        pdf.image LOGO, at: [380, 785], scale: 0.13

        pdf.bounding_box [pdf.bounds.right, pdf.bounds.top - 50], width: 300 do
          pdf.text @helper.new_line
        end

        accounts.each do |account|
          sum_eur  = 0
          sum_mmk  = 0
          donation = donations.select { |d| d.kontonr_nila == account.id }

          next if donation.empty?
          stock = stocks[account.id][year]

          table_data = []

          table_data << ['Buchungsdatum', 'Name', 'Vorname', 'Betrag Euro',
                         'Betrag MMK', 'Umrechnungskurs', 'Vorgang', 'Vorgang Pendant',
                         'Vorgang Art', 'AufwandsNr', 'Rechnungsdatum']

          donation.each do |d|
            data    = []
            contact = contacts.find { |a| a.id == d.contact_id }
            sum_eur += d.betrag_brutto.to_f
            sum_mmk += d.betrag_mmk.to_f

            data << d.buchungs_datum
            data << contact.name unless contact.nil?
            data << contact.vorname unless contact.nil?
            data << d.betrag_brutto
            data << d.betrag_mmk
            data << d.cr
            data << d.vorgang
            data << d.vorgang_kommentar
            data << d.vorgang_art
            data << d.aufwands_nummer
            data << d.rechnungsdatum

            table_data << data
          end

          table_data << ['Summe', '', '', sum_eur.round(2), sum_mmk.round(2), '', '', '', '', '', '']
          table_data << ['Anfangsbestand', '', '', stock[:eur_start], stock[:mmk_start], '', '', '', '', '', '']
          table_data << ['Endbestand', '', '', stock[:eur_end], stock[:mmk_end], '', '', '', '', '', '']

          tab = Prawn::Table.new(table_data, pdf, cell_style: { inline_format: true, border_width: 0.5,
                                                                font: 'Helvetica', size: 7, align: :center
                                                              })

          pdf.text account.name
          tab.draw
          pdf.text @helper.new_line 2
        end
      end
      File.basename(fn)
    end

    def export_donations(donations, year, kind)
      fn = File.join(FP, "#{kind}_#{year}.pdf")
      i  = 1
      new_page = []
      new_page = %w(4 8 12 16 20) if kind == 'Mittelherkunft'
      new_page = %w(3 7 11 15 19) if kind == 'Vermoegen'

      Prawn::Document.generate(fn, page_size: 'A4', right_margin: 5, left_margin: 15) do |pdf|
        pdf.font 'Helvetica'
        pdf.font_size 11

        pdf.image LOGO, at: [380, 785], scale: 0.13

        pdf.bounding_box [pdf.bounds.right, pdf.bounds.top - 50], width: 300 do
          pdf.text @helper.new_line
        end

        pdf.text [kind, year.to_s].join(' '), size: 14, align: :center, style: :bold, color: 'b9785c'

        donations.each do |name, d|
          next if d.empty?
          t_sum = 0
          table_data = []

          table_data << ['', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '<b>Summe</b>']

          d.each do |k, dk|
            data = []
            k_sum = 0

            data << k

            dk.each do |_, sum|
              k_sum += sum.to_f
              data << sum.round(2)
            end

            data << "<b>#{k_sum.round(2)}</b>"
            t_sum += k_sum
            table_data << data
          end

          data = ['<b>Summe</b>']

          (1..12).each do |month|
            data << "<b>#{d.sum { |_, dm| dm[month] }.round(2)}</b>"
          end

          data << "<b>#{t_sum.round(2)}</b>"

          table_data << data

          tab = Prawn::Table.new([["<b>#{name}</b>"]], pdf, cell_style: { inline_format: true, border_width: 0,
                                                                          font: 'Helvetica', size: 10
                                                                        })
          tab.draw

          tab = Prawn::Table.new(table_data, pdf, column_widths: [64, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 38, 50],
                                                  cell_style: { inline_format: true, border_width: 0.5,
                                                                font: 'Helvetica', size: 7, align: :center
                                                              })
          tab.draw

          pdf.text @helper.new_line 2

          pdf.start_new_page if new_page.include? i.to_s

          i += 1
        end
      end

      File.basename(fn)
    end
  end
end
